<?php
	function checkField($field){
		if(array_key_exists("data", $_SESSION)){
			if(array_key_exists($field, $_SESSION["data"])){
				echo $_SESSION["data"][$field];
				unset($_SESSION["data"][$field]);
			}
		}
	}
?>
<h1>New User</h1>
<?php
if(array_key_exists("errors", $_SESSION)){
	foreach($_SESSION["errors"] as $error){
		echo $error."<br>";
	}
	unset($_SESSION["errors"]);
}
?>
<form method="POST" action="index.php?page=create">
	<div>
		<label for="firstname">Firstname</label>
		<input id="firstname" type="text" placeholder="John" name="firstname" value="<?php checkField("firstname") ?>">
	</div>
	<div>
		<label for="lastname">Lastname</label>
		<input id="lastname" type="text" placeholder="Doe" name="lastname" value="<?php checkField("lastname") ?>">
	</div>
	<div>
		<label for="email">Email</label>
		<input id="email" type="text" placeholder="john.doe@domain.com" name="email" value="<?php checkField("email") ?>">
	</div>
	<div>
		<label for="phone">Phone</label>
		<input id="phone" type="text" placeholder="0711123456" name="phone" value="<?php checkField("phone") ?>">
	</div>
	<input type="submit">
</form>
<?php
if(array_key_exists("data", $_SESSION)){
	unset($_SESSION["data"]);
}
?>