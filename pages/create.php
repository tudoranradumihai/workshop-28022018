<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$validation = true;
	$errors = array();
	$data = array();

	$fields = array("firstname","lastname","email","phone");
	foreach($fields as $field){
		if(empty($_POST[$field])){
			$validation = false;
			array_push($errors,"Field '$field' is required.");
		} else {
			$data[$field] = $_POST[$field];
		}
	}

	if(array_key_exists("email", $data)){
		if(!filter_var($data["email"],FILTER_VALIDATE_EMAIL)){
			$validation = false;
			unset($data["email"]);
			array_push($errors,"Wrong email format.");
		}
	}

	if(array_key_exists("phone", $data)){
		if(!is_numeric($data["phone"])){
			$validation = false;
			unset($data["phone"]);
			array_push($errors,"Wrong phone format.");
		}
	}

	if($validation){
		$connection = mysqli_connect("localhost","root","","workshop");
		$query = "INSERT INTO users (firstname,lastname,email,phone) VALUES ('$data[firstname]','$data[lastname]','$data[email]','$data[phone]');";
		$result = mysqli_query($connection,$query);
		if($result){
			$id = mysqli_insert_id($connection);
			setcookie("identifier",$id,time()+3600);
			header("Location: index.php?page=list");
		} else {
			$_SESSION["errors"] = array("Database Error.");
			$_SESSION["data"]   = $data;
			header("Location: index.php?page=new");
		}
	} else {
		$_SESSION["errors"] = $errors;
		$_SESSION["data"]   = $data;
		header("Location: index.php?page=new");
	}




} else {
	header("Location: index.php?page=new");
}