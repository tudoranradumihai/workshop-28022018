<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include "partials/headerResources.php"; ?>
	</head>
	<body>
		<div class="container">
		<?php include "partials/header.php";
		if(array_key_exists("page", $_GET)){
			$path = "pages/".$_GET["page"].".php";
			if(file_exists($path)){
				include $path;
			} else {
				echo "<h1>404</h1>";
			}
		} else {
			include "pages/home.php";
		}
		include "partials/footer.php";?>
		</div>
		<?php include "partials/footerResources.php"; ?>
	</body>
</html>