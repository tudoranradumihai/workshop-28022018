<a href="list.php">BEC</a>
<?php
session_start();
if(array_key_exists("cart", $_SESSION)){
	if(count($_SESSION["cart"])>0){
		$cheiProduse = array_keys($_SESSION["cart"]);

		foreach($cheiProduse as $key => $value){
			$cheiProduse[$key] = "id=".$value;
		}
		$query = "SELECT * FROM products WHERE ".implode(" OR ",$cheiProduse);
		$connection = mysqli_connect("localhost","root","","aslpls");
		$result = mysqli_query($connection,$query);
		$products = array();
		while($product = mysqli_fetch_assoc($result)){
			$products[$product["id"]] = $product;
		}
		echo "<table>";
		$counter=0;
		$totalPrice = 0;
		foreach($_SESSION["cart"] as $key => $value){
			echo "<tr>";
			echo "<td>".++$counter."</td>";
			echo "<td>".$products[$key]["name"]."</td>";
			$itemPrice = $value*floatval($products[$key]["price"]);
			$totalPrice += $itemPrice;
			echo "<td>".$value."</td>";
			echo "<td><a href=\"deleteitem.php?id=".$key."\">Delete Item</a></td>";
			echo "<td align=\"right\">".number_format($itemPrice,2)."</td>";
			echo "</tr>";
		}

		echo "<tr>";
		echo "<td colspan=\"4\"></td>";
		echo "<td>".number_format($totalPrice,2)."</td>";				
		echo "</tr>";
		echo "</table>";
	} else {
			echo "Nu avem produse in cart.";
	}
} else {
	echo "Nu avem produse in cart.";
}