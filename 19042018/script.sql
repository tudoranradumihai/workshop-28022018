/*
CREATE TABLE nume_tabel (
	nume_coloana TIP(lungime)
);
*/

CREATE TABLE products (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255),
	description TEXT,
	price FLOAT(11.2),
	stock INT(11)
);

INSERT INTO products (name,description,price,stock)
VALUES ('iphone','ai de foioioi',999.90,5);