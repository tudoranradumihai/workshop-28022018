<?php
session_start();
if($_SERVER["REQUEST_METHOD"]=="POST"){
	if(array_key_exists("cart", $_SESSION)){
		if(array_key_exists($_POST["product"], $_SESSION["cart"])){
			$_SESSION["cart"][$_POST["product"]] += (int)$_POST["quantity"];
		} else {
			$_SESSION["cart"][$_POST["product"]] = (int)$_POST["quantity"];
		}
	} else {
		$_SESSION["cart"] = array(
			$_POST["product"] => intval($_POST["quantity"])
		);
	}
	header("Location: cart.php");
} else {
	header("Location: list.php");
}