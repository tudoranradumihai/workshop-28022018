<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
	if(!file_exists("config.php")){
		$file = fopen("config.php","w");
		$content = "<?php return '".json_encode($_POST)."';";
		fputs($file,$content);
		fclose($file);
	}
	if(isset($_POST["sync"])){

		$config = require "config.php";
		$config = json_decode($config,TRUE);
		$connection = mysqli_connect($config["hostname"],$config["username"],$config["password"],$config["database"]);
		$files = scandir("MySQL");
		$files = array_values(array_diff($files,array(".","..")));
		foreach($files as $file){
			$SQL = file_get_contents("MySQL/".$file);
			$result = mysqli_query($connection,$SQL);
			if($result){
				echo $file." OK<br>";
			} else {
				echo $file." NOK<br>";
			}
		}
	}
}

$config = require "config.php";
$config = json_decode($config,TRUE);
?>

<form method="POST">
	Hostname: <input type="text" name="hostname" value="<?php echo $config['hostname'];?>"><br>
	Username: <input type="text" name="username" value="<?php echo $config['username'];?>"><br>
	Password: <input type="text" name="password" value="<?php echo $config['password'];?>"><br>
	Database: <input type="text" name="database" value="<?php echo $config['database'];?>"><br>
	<input type="submit">
	<input type="submit" name="sync" value="SYNC">
</form>